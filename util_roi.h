#ifndef _UTIL_ROI_H_
#define _UTIL_ROI_H_

#include <opencv2/opencv.hpp>

namespace util
{

//
void warp_image(const cv::Mat &src, cv::Mat &dst, cv::Mat &m)
{
    CV_Assert(src.channels() == dst.channels());

    cv::Size win_size = dst.size();
    double matrix[6];
    cv::Mat M(2, 3, CV_64F, matrix);
    m.convertTo(M, CV_64F);
    double dx = (win_size.width - 1) * 0.5;
    double dy = (win_size.height - 1) * 0.5;
    matrix[2] -= matrix[0] * dx + matrix[1] * dy;
    matrix[5] -= matrix[3] * dx + matrix[4] * dy;

    printf("warp \n");
    CV_Assert(src.depth() == dst.depth());
    cv::warpAffine(src, dst, M, dst.size(),
                   cv::INTER_LINEAR + cv::WARP_INVERSE_MAP,
                   cv::BORDER_REPLICATE);
}
//----------------------------------------------------------
//
//----------------------------------------------------------
void get_rotated_ROI(cv::RotatedRect rr, cv::Mat &img, cv::Mat &dst)
{
    cv::Mat m(2, 3, CV_64FC1);
    float ang = rr.angle * CV_PI / 180.0;
    m.at<double>(0, 0) = std::cos(ang);
    m.at<double>(1, 0) = std::sin(ang);
    m.at<double>(0, 1) = -std::sin(ang);
    m.at<double>(1, 1) = std::cos(ang);
    m.at<double>(0, 2) = rr.center.x;
    m.at<double>(1, 2) = rr.center.y;
    warp_image(img, dst, m);
}

cv::Point2f rotate_point(float cx, float cy, float angle, cv::Point2f p)
{
    float theta = angle * CV_PI / 180.0f;
    float s = std::sin(theta);
    float c = std::cos(theta);

    // translate point back to origin:
    p.x -= cx;
    p.y -= cy;

    // rotate point
    float xnew = p.x * c - p.y * s;
    float ynew = p.x * s + p.y * c;

    // translate point back:
    p.x = xnew + cx;
    p.y = ynew + cy;
    return p;
}
};

#endif