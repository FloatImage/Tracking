#include <opencv2/opencv.hpp>
#include <vector>

#include "util_roi.h"
#include "kcf.h"

using namespace std;

cv::Scalar green = cv::Scalar(0, 255, 0);
cv::Scalar red = cv::Scalar(0, 0, 255);
cv::Scalar blue = cv::Scalar(255, 0, 0);

cv::Mat get_gaussian_kernel(int rows, int cols, double sigmax, double sigmay)
{
    auto gauss_x = cv::getGaussianKernel(cols, sigmax, CV_32F);
    auto gauss_y = cv::getGaussianKernel(rows, sigmay, CV_32F);
    return gauss_x * gauss_y.t();
}

//
inline double get_point_distance(cv::Point2f a, cv::Point2f b)
{
    return std::sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

// get angle between two vectors, c is center
inline double get_vector_angle(cv::Point2f a, cv::Point2f b, cv::Point2f c)
{
    double x1 = a.x - c.x;
    double y1 = a.y - c.y;
    double x2 = b.x - c.x;
    double y2 = b.y - c.y;
    double dot = x1 * x2 + y1 * y2; // dot product between [x1, y1] and [x2, y2]
    double det = x1 * y2 - y1 * x2; // determinant
    return std::atan2(det, dot);    // atan2(y, x) or atan2(sin, cos)
}

// transform search ROI back to original image
inline cv::Mat get_transform_rect_to_rotate(cv::RotatedRect rotated_rect)
{
    float w = rotated_rect.size.width;
    float h = rotated_rect.size.height;

    cv::Point2f rect_corners[4];
    rect_corners[0] = cv::Point(0, 0);
    rect_corners[1] = cv::Point(w, 0);
    rect_corners[2] = cv::Point(w, h);
    rect_corners[3] = cv::Point(0, h);

    cv::Point2f rotated_rect_corners[4];
    rotated_rect.points(rotated_rect_corners);

    return cv::getAffineTransform(rect_corners, rotated_rect_corners);
}

// transform tracked rect to original image
inline std::vector<cv::Point2f> transform_rect_points(cv::Rect rect, cv::Mat M)
{
    std::vector<cv::Point2f> src(4);
    std::vector<cv::Point2f> dst(4);
    src[0] = cv::Point2f(rect.x, rect.y);
    src[1] = cv::Point2f(rect.x + rect.width, rect.y);
    src[2] = cv::Point2f(rect.x + rect.width, rect.y + rect.height);
    src[3] = cv::Point2f(rect.x, rect.y + rect.height);
    cv::transform(src, dst, M);
    return dst;
}

//----------------------------------------------------------
//
//----------------------------------------------------------
int main(int argc, char *argv[])
{
    std::string cfg_file;
    if (argc == 2)
        cfg_file = argv[1];
    else
        cfg_file = "./cfg.yaml";

    cv::FileStorage fs(cfg_file, cv::FileStorage::READ);
    if (fs.isOpened() == false)
    {
        std::cout << "Usage: " << argv[0] << " config.yaml\n";
        return 1;
    }

    // //
    // cv::Mat gauss = get_gaussian_kernel(400, 200, 20, 30);
    // cv::normalize(gauss, gauss, 1, 0, cv::NORM_MINMAX);
    // cv::imshow("gauss", gauss);
    // cv::moveWindow("gauss", 0, 800);

    /////////////// read cfg file //////////////////////
    std::string filename;
    fs["image_filename"] >> filename;

    float img_resize_factor = 0.2;
    fs["img_resize_factor"] >> img_resize_factor;

    // hough circle param
    float hough_resize_factor = 0.15;
    float hough_param1 = 200;
    float hough_param2 = 200;
    float hough_min_radius = 100;
    fs["hough_resize_factor"] >> hough_resize_factor;
    fs["hough_param1"] >> hough_param1;
    fs["hough_param2"] >> hough_param2;
    fs["hough_min_radius"] >> hough_min_radius;

    float init_rect_pose_radius = -114;
    float init_rect_pose_angle = -3;
    int init_rect_width = 200;
    int init_rect_height = 64;
    float rect_self_rotation = 0;
    fs["init_rect_pose_radius"] >> init_rect_pose_radius;
    fs["init_rect_pose_angle"] >> init_rect_pose_angle;
    fs["init_rect_width"] >> init_rect_width;
    fs["init_rect_height"] >> init_rect_height;
    fs["rect_self_rotation"] >> rect_self_rotation;
    printf("image_filename: %s\n", filename.c_str());
    printf("init_rect pose: %f,%f\n", init_rect_pose_radius, init_rect_pose_angle);
    printf("init rect size: %d,%d\n", init_rect_width, init_rect_height);

    bool use_linearkernel = false;
    bool use_scale = false;
    fs["use_linearkernel"] >> use_linearkernel;
    fs["use_scale"] >> use_scale;

    float search_radius_min = 0;
    float search_radius_max = 0;
    float init_search_radius = 0;
    float search_roi_width = 0;
    float search_roi_height = 0;

    fs["search_radius_min"] >> search_radius_min;
    fs["search_radius_max"] >> search_radius_max;
    fs["init_search_radius"] >> init_search_radius;
    fs["search_roi_width"] >> search_roi_width;
    fs["search_roi_height"] >> search_roi_height;

    fs.release();

    ////////////// tracker /////////////////////////////
    KCF_Tracker tracker;
    tracker.m_use_linearkernel = use_linearkernel;
    tracker.m_use_scale = use_scale;

    // load image as color
    cv::Mat img = cv::imread(filename, 1);
    cv::Mat gray;
    cv::cvtColor(img, gray, CV_BGR2GRAY);

    ////////// find center /////////////////////////////
    std::vector<cv::Vec3f> circles;
    cv::Mat gray_resized, img_resized;
    cv::resize(img, img_resized, cv::Size(), hough_resize_factor, hough_resize_factor);
    cv::resize(gray, gray_resized, cv::Size(), hough_resize_factor, hough_resize_factor);

    cv::HoughCircles(gray_resized, circles, CV_HOUGH_GRADIENT, 1, hough_param1, hough_param2, hough_min_radius);
    for (int i = 0; i < circles.size(); i++)
    {
        cv::circle(img_resized, cv::Point(circles[i][0], circles[i][1]), circles[i][2], blue, 2);
        cv::circle(img_resized, cv::Point(circles[i][0], circles[i][1]), 1, red, 2);
    }

    cv::imshow("circle", img_resized);
    std::cout << "detected number of circles: " << circles.size() << std::endl;
    cv::waitKey(10);

    /////////// init rect for tracking /////////////////
    cv::Vec3f img_circle = 1.0 / hough_resize_factor * circles[0];
    printf("circle size: %f,%f,%f\n", img_circle[0], img_circle[1], img_circle[2]);
    cv::Point2f img_center = cv::Point2f(img_circle[0], img_circle[1]);

    cv::Point2f chip_center = cv::Point2f(img_circle[0], img_circle[1] + init_rect_pose_radius);
    cv::Point2f init_chip_center = util::rotate_point(img_circle[0], img_circle[1], init_rect_pose_angle, chip_center);

    // get template chip
    cv::RotatedRect chip(init_chip_center, cv::Size(init_rect_width, init_rect_height), rect_self_rotation);
    cv::Mat chip_template(chip.size, CV_8UC1);
    util::get_rotated_ROI(chip, gray, chip_template);
    cv::Point2f rect_points[4];
    chip.points(rect_points);
    for (int j = 0; j < 4; j++)
    {
        cv::line(img, rect_points[j], rect_points[(j + 1) % 4], blue, 2, CV_AA);
    }
    cv::imshow("chip template", chip_template);
    cv::moveWindow("chip template", 0, 100);

    //////////////// start searching /////////////////////
    cv::Rect init_rect(0, 0, chip_template.cols, chip_template.rows);
    tracker.init(chip_template, init_rect);

    // start from init rect
    float search_radius = init_rect_pose_radius;

    // R* delta_thetha ~= template_width
    float search_angle_step = init_rect_width / search_radius * 180 / CV_PI;
    float acculated_angle = 0;
    cv::Point2f curr_tracked_center = init_chip_center;
    cv::Point2f last_tracked_center = init_chip_center;
    float last_response = -1;
    float curr_response = -1;
    cv::Mat img_show = img.clone();
    bool continuous = false;
    for (int i = 0; i < 10000; i++)
    {
        printf("search angle step: %f\n", search_angle_step);

        cv::Point2f search_roi_center = util::rotate_point(img_circle[0], img_circle[1],
                                                           search_angle_step,
                                                           last_tracked_center);

        acculated_angle += search_angle_step;
        cv::RotatedRect search_roi(search_roi_center,
                                   cv::Size(search_roi_width, search_roi_height),
                                   rect_self_rotation + acculated_angle);

        // new search window
        cv::Point2f rect_points[4];
        search_roi.points(rect_points);
        cv::Mat roi_image(search_roi.size, CV_8UC1);
        util::get_rotated_ROI(search_roi, gray, roi_image);

        // track
        double tic = cv::getTickCount();
        tracker.track(roi_image);
        tic = ((double)cv::getTickCount() - tic) * 1000. / cv::getTickFrequency();
        printf("time cost: %f ms\n", tic);

        curr_response = tracker.get_max_response();
        if (i == 0)
        {
            last_response = curr_response;
        }
        else
        {
            if (curr_response * 2 < last_response)
            {
                printf("Track error probabiliy is high! Current tracked response is too low!\n");
                cv::imwrite("result.jpg", img_show);
                break;
            }
        }

        BBox_c bb = tracker.getBBox();
        cv::Rect result = bb.get_rect();
        printf("tracked bb[cx,cy,w,h]: %f,%f,%f,%f\n ", bb.cx, bb.cy, bb.w, bb.h);
        printf("max response: %f\n", tracker.get_max_response());

        ///// find tracked rect
        float roi_cx = (float)roi_image.cols / 2.0f;
        float roi_cy = (float)roi_image.rows / 2.0f;
        float dx = bb.cx - roi_cx;
        float dy = bb.cy - roi_cy;
        cv::Point2f dxy = util::rotate_point(0, 0, rect_self_rotation + acculated_angle, cv::Point2f(dx, dy));
        cv::Point2f tracked_center = search_roi_center + dxy;
        cv::Point2f tracked_points[4];

        // tracked rotated rect for visualization
        cv::RotatedRect tracked_rect(tracked_center, cv::Size(bb.w, bb.h), rect_self_rotation + acculated_angle);
        tracked_rect.points(tracked_points);

        // convert to radius, angle
        float track_radius = get_point_distance(img_center, tracked_center);
        printf("track radius: %f\n", track_radius);

        curr_tracked_center = tracked_center;

        float track_angle = get_vector_angle(last_tracked_center, curr_tracked_center, img_center);
        track_angle = track_angle * 180 / CV_PI;
        printf("track angle: %f\n", track_angle);

        // update track radius and angle
        last_tracked_center = tracked_center;
        search_angle_step = track_angle;
        search_radius = track_radius;

        // visualize
        if (!continuous)
        {
            cv::Mat roi_color;
            cv::cvtColor(roi_image, roi_color, CV_GRAY2BGR);
            cv::rectangle(roi_color, result, CV_RGB(0, 255, 0), 3);
            cv::imshow("track", roi_color);
            cv::moveWindow("track", 0, 500);
        }
        // cv::Mat response = tracker.get_response_mat();
        // if (response.data)
        // {
        //     cv::normalize(response, response, 1, 0, cv::NORM_MINMAX);
        //     cv::imshow("response", response);
        //     cv::moveWindow("response", 0, 700);
        // }

        cv::circle(img_show, tracked_center, 2, red, 2);
        for (int j = 0; j < 4; j++)
        {
            // cv::line(img_show, rect_points[j], rect_points[(j + 1) % 4], green, 2, CV_AA);
            cv::line(img_show, tracked_points[j], tracked_points[(j + 1) % 4], blue, 3, CV_AA);
        }
        // cv::circle(img_show, search_roi_center, 2, green);
        // cv::circle(img_show, img_center, 2, red);
        // cv::line(img_show, img_center, search_roi_center, green, 2);

        if (!continuous)
        {
            cv::Mat resized_show;
            cv::resize(img_show, resized_show, cv::Size(), img_resize_factor, img_resize_factor);

            cv::imshow("src", resized_show);
            cv::moveWindow("src", 600, 0);
            cv::imshow("search_roi", roi_image);
            cv::moveWindow("search_roi", 0, 300);
        }

        if (!continuous)
        {
            int key = cv::waitKey();
            if (key == 'c')
                continuous = true;
            else if (key == 27)
                break;
        }
        else
        {
            int key = cv::waitKey(1);
            if(key == 'c')
                continuous = false;
            else if(key == 27)
                break;
        }
    }
    cv::destroyAllWindows();
    return 0;
}